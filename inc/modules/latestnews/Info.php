<?php

    return [
        'name'          =>  $core->lang['latestnews']['module_name'],
        'description'   =>  $core->lang['latestnews']['module_desc'],
        'author'        =>  'KrystianStd',
        'version'       =>  '1.3',
        'icon'          =>  'newspaper-o',

        'install'       =>  function() use($core)
        {
            $core->db()->pdo()->exec("CREATE TABLE IF NOT EXISTS `latestnews` (
                `id` integer NOT NULL PRIMARY KEY AUTOINCREMENT,
                `field` text NOT NULL,
                `value` text NOT NULL
            )");

            $core->db()->pdo()->exec("INSERT INTO `latestnews` (`field`, `value`) VALUES ('amount', '3')");
            $core->db()->pdo()->exec("INSERT INTO `latestnews` (`field`, `value`) VALUES ('author', 'no')");
            $core->db()->pdo()->exec("INSERT INTO `latestnews` (`field`, `value`) VALUES ('images', 'yes')");
            $core->db()->pdo()->exec("INSERT INTO `latestnews` (`field`, `value`) VALUES ('title', 'yes')");
            $core->db()->pdo()->exec("INSERT INTO `latestnews` (`field`, `value`) VALUES ('date', 'no')");
            $core->db()->pdo()->exec("INSERT INTO `latestnews` (`field`, `value`) VALUES ('text', 'yes')");
            $core->db()->pdo()->exec("INSERT INTO `latestnews` (`field`, `value`) VALUES ('custom', 'yes')");
            $core->db()->pdo()->exec("INSERT INTO `latestnews` (`field`, `value`) VALUES ('cut', '20')");
        },

        'uninstall'     =>  function() use($core)
        {
            $core->db()->pdo()->exec("DROP TABLE `latestnews`");
        }
    ];
