var fullWindow;

fullWindow = new function () {

  //private vars
  var breakpoint = 1024;

  //catch DOM
  var $height;
  var $width;
  var $objects;

  //bind events
  $(document).ready(function () {
    $objects = $('.fullWindow');


    if ($(window).width() < breakpoint) {
      changeHeight();
    }
  });

  $(window).bind('resize orientationchange', function () {

    if ($(window).width() != $width) {
      changeHeight();
    }

    if ($(window).width() >= breakpoint) {
      resetHeight();
    }
  });

  //private functions
  var changeHeight = function () {
    $height = $(window).height();
    $width = $(window).width();

    $objects.css({'height': $height});
  };

  var resetHeight = function () {
    $objects.removeAttr('style');
  };
};