var map;

map = new function () {

  //private vars
  var map;
  var customMarker;
  var myLatLng;
  var options;

  //catch DOM
  var $canvas = document.getElementById("map__canvas");

  //bind events
  $(window).bind('resize', function () {
    setTimeout(function () {
      map.setCenter(myLatLng);
    }, 0);
  });

  //public functions
  this.loadApi = function () {
    var script = document.createElement("script");
    script.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyBLlnLLh8HK7GRvGfX9bHeBk_Xa2G_1x50';
    document.body.appendChild(script);
  };

  this.init = function (latMap, lngMap, zoomMap, styles) {
    setOptions(latMap, lngMap, zoomMap, styles);
    setMap();
  };

  //private functions
  var setOptions = function (latMap, lngMap, zoomMap, styles) {
    myLatLng = new google.maps.LatLng(latMap, lngMap);

    options = {
      zoom: zoomMap,
      center: myLatLng,
      scrollwheel: false,
      mapTypeControl: false,
      streetViewControl: false,
      styles: styles
    };
  };

  var setMap = function () {
    CustomMarker.prototype = new google.maps.OverlayView();
    function CustomMarker(latlng, map, args) {
      this.latlng = latlng;
      this.args = args;
      this.setMap(map);

      this.draw = function () {
        var self = this;

        var div = this.div;

        if (!div) {

          div = this.div = document.getElementById('map__marker');

          if (typeof(self.args.marker_id) !== 'undefined') {
            div.dataset.marker_id = self.args.marker_id;
          }

          google.maps.event.addDomListener(div, "click", function (event) {
            google.maps.event.trigger(self, "click");
          });

          var panes = this.getPanes();
          panes.overlayImage.appendChild(div);
        }

        var point = this.getProjection().fromLatLngToDivPixel(this.latlng);

        if (point) {
          div.style.left = point.x + 'px';
          div.style.top = point.y + 'px';
        }
      }
    }

    if ($canvas) {
      map = new google.maps.Map($canvas, options);
      customMarker = new CustomMarker(myLatLng, map, {id: "marker1"});
    }
  };
};

map.loadApi();