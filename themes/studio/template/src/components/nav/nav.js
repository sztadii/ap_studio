var nav;

nav = new function () {

  //private functions
  var breakpoint = 1024;

  //catch DOM
  var $windowWidth;
  var $button;
  var $background;
  var $listMain;

  //bind events
  $(document).ready(function () {
    init();
  });

  $(window).resize(function () {
    $windowWidth = $(window).width();
  });

  //private functions
  var init = function () {
    catchVars();
    triggerNav();
  };

  var catchVars = function () {
    $windowWidth = $(window).width();
    $button = $('.nav__button');
    $background = $('.nav .nav__background');
    $listMain = $('.nav .nav__list');
  };

  var triggerNav = function () {
    $button.on('click', function () {
      toggleNav();
    });

    $background.on('click', function () {
      toggleNav();
    });

    $('.nav__link').on('click', function () {
      toggleNav();
    });
  };

  var hideNav = function () {
    $background.fadeOut(500);
    $listMain.fadeOut(300, function () {
      $button.removeClass('-active');
    });
  };

  var showNav = function () {
    $background.fadeIn(500);
    $listMain.fadeIn(300, function () {
      $button.addClass('-active');
    });
  };

  var toggleNav = function () {
    if (breakpoint > $windowWidth) {
      if ($button.hasClass('-active')) {
        hideNav();
      } else {
        showNav();
      }
    }
  };
};